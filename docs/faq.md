# Frequently Asked Questions

## General
### What will the Foundry DSO service provide?

The PREDA project is delivering a centralised DSO service based on Red Hat OpenShift, offering DSO tools, capabilities, and assets to improve and accelerate the development and deployment of application software.

The service will exploit Cloud-Native Application Platform (CNAP) capabilities to provide an integrated suite of technologies and modern applications that are fast to deploy and easy to update. MOD GitHub will serve as the Source Code management repository.

### Who will the service be available to?

PREDA’s ambition is to serve all those who provide software for Defence, across our enterprise and our industry partners.

The Foundry DSO service will provide industry best practice agile development, test and production environments to MOD DSO teams, and will provide capability to teams currently without access to MOD DSO resources.

The service will also be utilised by relevant industry partners working with MOD.

### What will initially be available and how will this evolve over time?

The first phase of the project is focused on the delivery of a successful Minimum Viable Product (MVP) Alpha launch of the RedHat OpenShift Container Platform with DSO tooling by January 2022.

The service will be launched across Defence in April 2022, having been tested by two initial MOD application teams as the first proof of value in private Beta stage.

## Budget / Funding
### What will be centrally funded by MOD, and what will be funded by the user?

The PREDA project has been allocated funding to develop and deliver the initial Foundry DSO service, with the overall service cost model still to be agreed.

Our ambition is for the service to run centrally, with some elements funded by projects on an individual basis. A proof of value business case for using the service has been developed, with a complete set of user requirements currently in development.

### Will the service make MODCloud more cost-effective through-life than on-prem solutions or direct arrangements with cloud providers (noting that the service provides more than just hosting capabilities)?

The Foundry DSO service is based on DSO CI/CD pipeline with standardised enterprise software agreements.

This means that users will benefit from a flexible, secure, and assured pipeline that delivers reusable technologies and economies of scale to minimise the time between development, test, and higher environments.

## Technology / Tools
### What DevSecOps tooling will the service provide?

The Foundry DSO Service offers a number of MVP capabilities to MOD development teams, including DSO Tools for application build, test and deployment.

The project team continues to work with users to understand existing DSO toolsets and establish target tooling in line with assurance.

### Will the Foundry DSO service support cloud native DevSecOps?

Yes. We are using RedHat OpenShift Container Platform (OCP) on MODCloud at OFFICIAL, and are targeting the use of Azure Stack Hub (ASH) on-prem at SECRET in the future.

### Will dev/test, pre-prod and production environments be made available?

Dev/test & pre-prod environments will be available at OFFICIAL via ModCloud iACE. A production environment will be available at SECRET.

### Are there specific patterns that will be supported? When will these be published and when will they go live?

It is our ambition to have a catalogue of application templates accessible via GitHub that can be used to accelerate development through self-service. These will be available over time as the service evolves.

### Does PREDA provide hosting services?

The Foundry DSO service is a DevSecOps CI/CD pipeline. Apps that do not need the development pipeline should consider looking at MODCloud hosting services.

## Access
### Will the service enable access via MODNet only?

Initially, access to the service will be via the MODCloud ACE environment for MOD users and relevant industry partners.

The project’s long-term ambition is to provide a platform-agnostic environment which will be available to all relevant teams from appropriate locations and networks.

### How will I access the service?

The Foundry DSO service, onboarding and documentation will be accessed via a ‘front door’ Community Site. The project team will work collaboratively with the user community to develop an MVP Community Site, to understand where it should be hosted and what it needs to contain.

Our long-term ambition is to provide an extensive ‘front-door’ offering with a comprehensive set of user requirements, with the aim to maximise community engagement and drive adoption of the service.

## Security
### What is Secure by Design?

A Secure by Design approach aims to provide a more comprehensive, end-to-end security solution through gap analysis using industry standard frameworks relevant to the service.

This approach does not replace the previous method of security accreditation instead it incorporates it as part of a more holistic security solution, tailored to the specific technical and business requirements of the project.

The primary change in this approach is that the Cyber Security Assessment and Advisory Service (CySAAS) will no longer ‘accredit’, instead they will perform the independent assessor function, providing a written ‘opinion’ on project security status, articulated as risk.

It will be for the project SRO to determine risk appetite and make the go/no go decision for the project to proceed to the next stage.

### What level of assurance will be provided centrally by the service?

The Foundry DSO service will be assured as part of Secure by Design. App owners will be responsible for their own assurance, with the aim to minimise the requirement for individual project assurance processes through the provision of inherited assurance.

This means that when applications are developed using the service in its entirety, they will inherit the majority of JSP 604 assurance.

### What circumstances will assurance not be provided centrally?

Only in certain instances where application development deviates from the integrity of the service pipeline, will further assurances be required. This will be made clear for re-use by teams as part of the onboarding process. Inherited assurance – parent assurance provided.

Such use cases will be determined as part of the onboarding process for MOD DSO teams, and will be managed on a case-by-case basis.

### What flow of code and data across security classifications will be permitted?

The Foundry DSO service will utilise a Cross-Domain Solution (CDS) that will be developed over time to allow the flow of code and data at OFFICIAL and from OFFICIAL to SECRET.


## Support and Training
### What support will be provided for onboarding teams and migration of applications onto the service?

Two early adopter teams will be carefully selected by the project team based on technical alignment and suitability, strategic fit and development team experience.

In parallel, the project team will be consulting the DSO community in order to develop a target roadmap of likely users of the service upon launch. In the future, it is expected that teams will be able to commission their own projects through a self-service onboarding experience.

### How will my team be supported to upskill on using the service?

Relevant training content will be available during onboarding and embedded throughout the platform lifecycle to provide access to the knowledge that users require at point of need and in a modern way.

The project is working with industry partners Red Hat to develop an immersive Dojo experience through providing first-hand access to DSO innovation capabilities.

## More Information
### Who can I contact to find out more?

We are working with DSO teams across MOD to leverage existing communities and grow a sustainable Foundry DSO Community of Practice. If you wish to register your interest in getting involved, please contact John Marshall and Michael Mcalister.
