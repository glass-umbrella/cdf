# The PREDA Project

Defence’s vision is to put modern digital capability at the heart of how it operates. The **[Modernising Defence Programme (MDP)](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/931705/ModernisingDefenceProgramme_report_2018_FINAL.pdf)** was proposed under the 2021 Integrated Review to provide a step-change in how Defence exploits digital, data and technology and prepare Defence for a Digital future.

The **[Digital Foundry](https://defencedigital.blog.gov.uk/category/digital-foundry/)** is currently being established with the aim of accelerating the delivery of common platforms and services to exploit data, digital and AI. It will form a federated partnership between digital innovators across Defence that pioneer new ways to deliver outcomes with pace and agility. **PREDA (Platform for Rapid Exploitation of Digital Applications) is one of the first projects that will deliver Digital Foundry capabilities through providing the Foundry DevSecOps service.**

## What is the context for PREDA?

Defence, like all modern enterprises, relies on processing information that is ever growing in volume and complexity, in a way that creates advantage. The practices and tools for software development that have emerged in response to this context emphasise creating applications at pace, with changes delivered to end users in hours at relatively low cost and risk.

These practices, drawn from DevSecOps, Agile software development and microservice architecture among others are widespread in leading technology companies and gaining significant traction with NATO, coalition partners and other government departments.

Adopting these tools and practices represents an opportunity for critical Defence software to become more like the digital products and services we see in other walks of life, that flex to the evolving context and changing needs of operational commanders.

## When was PREDA launched?

Although publicly announced in March 2019, subsequent changes in ministerial leadership and re-allocation of funding led to the PREDA project only gaining real momentum in 2021.

The consortium supplier team (PA Consulting and Accenture) mobilised in September 2021, with focus turning rapidly to design and delivery of the Foundry DSO service.

## What is PREDA focusing on?

PREDA will enable Defence to prevent or win in conflict using software created and fine-tuned for the challenges of the moment and needs of the user.

It will lay the engineering foundations for truly digital services to be brought to the frontline and to wider Defence, through identifying and exploiting opportunities related to people, process, technology and information, that will improve and accelerate the development and deployment of software.

## Where does PREDA sit within MoD?

PREDA is being delivered within Defence Digital, as one of the first projects that will deliver Digital Foundry capabilities through providing the Foundry DSO service. Supported by other Foundry partners across Defence, it will play a critical role in developing the Digital Backbone for Defence and industry partners alike. The project’s SRO is John Fitzpatrick, CIO Director of Digital Enablement and is aligned to Digital Foundry governance.
