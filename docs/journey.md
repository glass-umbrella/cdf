# Journey to Launch the Foundry DSO Service
Demand for the Foundry DevSecOps service has been building since the announcement of PREDA funding. The service will be launched across Defence in April 2022, having been tested by a minimum of two MOD application teams as the first ‘proof of value’ in private Beta stage, in line with **[GDS standards on agile delivery](https://www.gov.uk/service-manual/agile-delivery)**.

The two applications to utilise the service first will be carefully selected by the project team based on technical alignment and suitability, strategic fit and development team experience. In parallel, the project team will be working collaboratively with the user community to deliver the public Beta service from April 2022.

![Journey Flow](https://glassumbrellapublicbucket.s3.eu-central-1.amazonaws.com/Journey+Flow.png)

## Introduction to the Foundry DSO Service

The PREDA project is delivering a **centralised DevSecOps service** offering DSO tools, capabilities, and assets to **improve and accelerate the development and deployment of application software across classifications.** The Foundry DSO service, which is **based on Red Hat OpenShift Container Platform**, has been developed as a **Cloud-Native Application Platform** (CNAP), providing an integrated suite of technologies to create modern applications that are fast to deploy and easy to update. MOD GitHub will serve as the Source Code management repository.

![Process Flow](https://glassumbrellapublicbucket.s3.eu-central-1.amazonaws.com/Foundry+DSO+Service.png)

## Test & Dev Capabilities

The **initial** Foundry DSO Test&Dev Service on iACE offers the following capabilities to MoD development teams. User research will inform how the service capabilities evolve over time to meet user needs and requirements.

- Foundry DSO Community Site as the front-door to the Foundry DSO Service
- Source Code Repo (MoD GitHub SaaS)
- DevSecOps Tools for application build, test and deployment
- Container Platform based on RH OpenShift
- Monitoring and Logging Tools for Observability
- Container Image library for building microservice-based application
- Database container images (PostgreSQL, MongoDB) that can be deployed in application architectures
- RH AMQ (Kafka) containers images that can be included in application architecture
- File, Block, Object storage options for application persistent data
- Public endpoints for making applications accessible to the Internet
- RH Service Mesh (based on Istio)

## Technology Stack

The **initial** Foundry DSO Test&Dev Service on iACE offers the following technology stack, which will evolve over time in line with user research.

![Reference Architecture](https://glassumbrellapublicbucket.s3.eu-central-1.amazonaws.com/techstack.png)

## Proposed Toolchain

Similarly, the DevSecOps pipeline toolchain will evolve to meet user needs.

![DevSecOps Pipeline ToolChain](https://glassumbrellapublicbucket.s3.eu-central-1.amazonaws.com/toolchain.png)
