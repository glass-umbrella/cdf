# Welcome to Foundry DevSecOps Community Site

This site is no longer active

This is a test site hosted on [GitLab Pages](https://pages.gitlab.io) and built using [MKDocs](https://mkdocs.org).  You can [browse its source code](https://gitlab.com/glass-umbrella/preda-landing-page), fork it and start using it on your projects.  

MKDocs is great for treating technical documentation as code!  Even though this is a MVP, we can reuse throughout the program to update our docs.

We welcome your [feedback](https://ozf4d0kv63i.typeform.com/to/e9kBIiWu)
